const http = require('http');
const handlers = {};
const mongojs = require('mongojs');
// const db = mongojs('unpwdb');
const sha1 = require('sha1');
const user = "lifebean";
const pass = "lifebean";
const host = "35.221.67.213";
// const host = "10.146.0.2";
// const host = "127.0.0.1";
const port = "27017";
const dbas = "unpwdb";
let db = mongojs(user+":"+pass+"@"+host+":"+port+"/"+dbas);


handlers['/'] = (req, res) => {
	res.writeHead(200, { "Content-Type" : "text/html" });
	res.end("Hello World");
}

handlers['/login'] = (req, res) => {
	console.log("/login accessed");
	res.writeHead(200, {"Content-Type" : "application/json"});
	let comp = "";
	req.on('data', (resp) => {
		comp += resp.toString();
	});
	req.on('end', () => {
		let d = JSON.parse(comp);
		db.users.findOne({ username : d.un, password : d.pw }, (err, docs) => {
			if(docs == null) {
				res.end(JSON.stringify({ "result" : "invalid"}));
				console.log("invalid")
			}
			else {
				res.end(JSON.stringify({ "result" : "valid"}));
				console.log("valid")
			}
		});
	});
}

// handlers['/register'] = (req, res) => {
// 	let comp = "";
// 	req.on('data', (resp) => {
// 		comp += resp;
// 	});
// 	req.on('end', () => {
// 		let d = JSON.parse(comp);
// 		if(d.un != 0) {
// 			db.users.findOne({ username : d.un }, (err, docs) => {
// 				if(docs != null) {
// 					res.end("Username already exists!");
// 				}
// 				else {
// 					let p = sha1(d.pw);
// 					db.users.insert({ 
// 						username : d.un,
// 						password : p,
// 						firstname : d.fn,
// 						lastname : d.ln,
// 						birthdate : d.bd,
// 						bio : d.b,
// 						email : d.em
// 					});
// 					res.end("Registration complete!");
// 				}
// 			});
// 		}
// 		else {
// 			res.end("Please enter a valid username.");
// 		}
// 	});
// }

const server = http.createServer((req, res) => {
	if(handlers[req.url]) {
		handlers[req.url](req, res);
	}
	else {
		res.writeHead(404, { "Content-Type" : "text/html" });
		res.end("<h1>404: Page Not Found</h1>");
	}
}).listen(6766);

console.log("Server is running at port 6766..");
