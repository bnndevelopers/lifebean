package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.CalendarView;
import android.widget.Toast;
import android.widget.Button;
import android.view.View;
import android.graphics.Color;
import android.widget.CalendarView.OnDateChangeListener;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.LayoutInflater;
import android.content.Intent;
import android.widget.ListView;
import java.util.ArrayList;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

public class JournalMainScreen extends AppCompatActivity
{
	CalendarView calendar;
	ListView listview;
	ArrayList<JournalItem> list;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.journal_main);
		Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	 	getSupportActionBar().setTitle("Journal");
		calendar = (CalendarView)findViewById(R.id.calendar1);
		calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
			@Override
			public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
				Toast.makeText(getApplicationContext(), "Selected date " + dayOfMonth + "/" + month + "/" + year, Toast.LENGTH_SHORT).show();
			}
		});
		listview = (ListView)findViewById(R.id.listView);
		list = new ArrayList<JournalItem>();
		list.add(new JournalItem("task", "task1"));
		list.add(new JournalItem("task", "task2"));
		list.add(new JournalItem("task", "task3"));
		list.add(new JournalItem("note", "note"));
		list.add(new JournalItem("event", "event"));
		list.add(new JournalItem("note", "note"));
		list.add(new JournalItem("event", "event"));
		list.add(new JournalItem("note", "note"));
		list.add(new JournalItem("event", "event"));
		JournalArrayAdapter adapter = new JournalArrayAdapter(this, list);
		listview.setAdapter(adapter);
		Button addTaskBtn = (Button)findViewById(R.id.addTaskBtn);
		addTaskBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), AddTask.class);
				startActivity(intent);
			}
		});
		Button addNoteBtn = (Button)findViewById(R.id.addNoteBtn);
		addNoteBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), AddNote.class);
				startActivity(intent);
			}
		});
		Button addEventBtn = (Button)findViewById(R.id.addEventBtn);
		addEventBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), AddEvent.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
