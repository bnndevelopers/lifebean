package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.view.View;
import android.widget.EditText;
import android.view.Gravity;
import android.graphics.Color;
import android.widget.ImageView;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.widget.TextView;
import android.widget.Button;
import android.util.DisplayMetrics;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.content.Intent;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ScrollView;

public class WalletMonthExpenseScreen extends AppCompatActivity
{
	Date date = Calendar.getInstance().getTime();
	int screenHeight = 0;
	int screenWidth = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TextViewMaker tvm = new TextViewMaker(this);
		EditTextMaker etm = new EditTextMaker(this);
		ButtonMaker btm = new ButtonMaker(this);
		LinearLayout layout = new LinearLayout(this);
		DisplayMetrics dispMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenWidth = dispMetrics.heightPixels;
		layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		layout.setOrientation(LinearLayout.VERTICAL);
		LayoutInflater inflater = LayoutInflater.from(this);
		ScrollView scroll = new ScrollView(this);
		Toolbar toolbar = (Toolbar)inflater.inflate(R.layout.toolbar_layout, null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Monthly Expense");
		toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		/*Current Month TextView*/
		LinearLayout screenmonthtext = new LinearLayout(this);
		LinearLayout.LayoutParams monthtextparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		screenmonthtext.setLayoutParams(monthtextparams);
		monthtextparams.setMargins(30,100,0,50);
		TextView monthtext = tvm.createTextView(createMonth());
		monthtext.setTextSize(20);
		screenmonthtext.addView(monthtext);
		/*Current Expense Text*/
		LinearLayout screenexpenses = new LinearLayout(this);
		LinearLayout.LayoutParams currentwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		screenexpenses.setLayoutParams(currentwrap);
		currentwrap.setMargins(30,0,0,10);
		TextView current = tvm.createTextView("Current Expenses: ");
		current.setTextSize(15);
		screenexpenses.addView(current);
		/*Current Expenses Total Text*/
		LinearLayout screentotal = new LinearLayout(this);
		LinearLayout.LayoutParams totalwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		screentotal.setLayoutParams(totalwrap);
		totalwrap.setMargins(30,0,0,10);
		TextView total = tvm.createTextView("Total");
		total.setTextSize(15);
		screentotal.addView(total);
		/*Add and Cancel Buttons*/
		double limit = dispMetrics.widthPixels * 0.8;
		LinearLayout screenbutton = new LinearLayout(this);
		screenbutton.setGravity(Gravity.BOTTOM);
		LinearLayout.LayoutParams forscreenbutton = new LinearLayout.LayoutParams((int)limit, LinearLayout.LayoutParams.MATCH_PARENT);
		forscreenbutton.setMargins(0,0,0,50);
		forscreenbutton.gravity = Gravity.CENTER;
		screenbutton.setLayoutParams(forscreenbutton);
		RelativeLayout rl = new RelativeLayout(this);
		Button add = btm.createButton("Add Plan");
		add.setTextSize(12);
		add.setWidth((int)(screenWidth * 0.2));
		add.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), WalletAddBudgetScreen.class);
				startActivity(intent);
			}
		});
		rl.addView(add);
		LayoutParams addparams = (LayoutParams)add.getLayoutParams();
		addparams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		Button compare = btm.createButton("Compare Last Month");
		compare.setTextSize(12);
		compare.setWidth((int)(screenWidth * 0.2));
		compare.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), WalletComparisonScreen.class);
				startActivity(intent);
			}
		});
		rl.addView(compare);
		LayoutParams compareparams = (LayoutParams)compare.getLayoutParams();
		compareparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		rl.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		screenbutton.addView(rl);
		layout.addView(toolbar);
		layout.addView(screenmonthtext);
		layout.addView(screenexpenses);
		layout.addView(screentotal);
		layout.addView(screenbutton);
		scroll.addView(layout);
		setContentView(scroll);
	}

	public String createMonth() {
		SimpleDateFormat df = new SimpleDateFormat("MMMM, dd");
		String formattedDate = df.format(date);
		return(formattedDate);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
