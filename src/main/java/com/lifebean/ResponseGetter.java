package com.lifebean;

import java.net.HttpURLConnection;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.lang.StringBuilder;

public class ResponseGetter
{
	public static String lifebeanUrl = "https://life-bean.appspot.com/";

	public static String getResponse(HttpURLConnection urlConnection) throws Exception {
		StringBuilder sb = new StringBuilder();
		InputStream is = null;
		try {
			is = urlConnection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = "";
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			if(urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return(sb.toString());
	}
}
