package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.util.DisplayMetrics;
import android.graphics.Color;
import android.content.Context;

public class ButtonMaker
{
	int screenHeight = 0;
	int screenWidth = 0;
	Context con;

	public ButtonMaker(Context ctx) {
		con = ctx;
		DisplayMetrics dispMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenWidth = dispMetrics.heightPixels;
		screenHeight = dispMetrics.widthPixels;
	}

	public Button createButton(String btntext) {
		Button btn = new Button(con);
		btn.setTextSize(10);
		btn.setTextColor(Color.parseColor("#ffffff"));
		btn.setBackgroundColor(Color.parseColor("#669900"));
		btn.setText(btntext);
		return(btn);
	}
}
