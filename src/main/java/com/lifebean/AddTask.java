package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Button;
import android.content.Intent;
import android.view.View;
import android.app.DatePickerDialog;
import android.widget.TextView;
import java.util.Calendar;
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
import android.widget.DatePicker;
import android.widget.EditText;
import android.app.TimePickerDialog;
import android.widget.TimePicker;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class AddTask extends AppCompatActivity
{
	private EditText taskTitle, selectDate;
	private DatePickerDialog.OnDateSetListener dateSetListener;
	private Calendar calendar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_task);
		Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Add Task");
		taskTitle = (EditText) findViewById(R.id.taskTitle);
		selectDate = (EditText) findViewById(R.id.selectDate);
		selectDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				System.out.println("display time here");
			}
		});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
