package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import java.util.Timer;
import java.util.TimerTask;
import android.widget.ImageView;
import android.widget.LinearLayout;
import 	android.graphics.Color;

public class MainActivity extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout layout = new LinearLayout(this);
		ImageView bgSplash = new ImageView(this);
		bgSplash.setImageResource(R.drawable.lifebean_logo);
		layout.addView(bgSplash);
		layout.setBackgroundColor(Color.rgb(204, 204, 153));
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				startActivity(new Intent(getApplicationContext(), LoginScreen.class));
				finish();
			}
		}, 500);
		setContentView(layout);
	}
}
