package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.content.Intent;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.widget.Toast;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;

public class MainMenuScreen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
	private DrawerLayout drawer;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
		NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.nav_about: {
				startActivity(new Intent(this, AboutScreen.class));
				break;
			}
			case R.id.nav_privacy_policy: {
				startActivity(new Intent(this, PrivacyPolicyScreen.class));
				break;
			}
		}
		drawer.closeDrawer(GravityCompat.START);
		return(true);
	}

	@Override
	public void onBackPressed() {
		if(drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else {
			super.onBackPressed();
		}
	}

	public void showWalletScreen(View view) {
		startActivity(new Intent(this, WalletMainScreen.class));
	}

	public void showJournalScreen(View view) {
		startActivity(new Intent(this, JournalMainScreen.class));
	}

	public void showHealthScreen(View view) {
		startActivity(new Intent(this, HealthMainScreen.class));
	}

	public void showWorkoutScreen(View view) {
		startActivity(new Intent(this, WorkoutMainScreen.class));
	}
}
