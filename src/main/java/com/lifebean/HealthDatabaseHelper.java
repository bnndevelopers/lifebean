package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;

public class HealthDatabaseHelper extends SQLiteOpenHelper
{
	public static final String DATABASE_NAME = "bmi.db";
	public static final String TABLE_NAME = "bmi_table";
	public static final String COL1 = "ID";
	public static final String COL2 = "DATE";
	public static final String COL3 = "HEIGHT";
	public static final String COL4 = "WEIGHT";
	public static final String COL5 = "BMIRESULT";

	public HealthDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String createTable = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " + " DATE TEXT, HEIGHT TEXT, WEIGHT TEXT, BMIRESULT TEXT)";
		db.execSQL(createTable);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP IF TABLE EXISTS " + TABLE_NAME);
		onCreate(db);
	}

	public boolean addData(String date, String height, String weight, String bmiresult) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL2,date);
		contentValues.put(COL3,height);
		contentValues.put(COL4,weight);
		contentValues.put(COL5,bmiresult);

		long result = db.insert(TABLE_NAME, null, contentValues);
		
		if(result == -1) {
			return false;
		}
		else {
			return true;
		}
	}

	public Cursor showData() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor data = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "'", null);
		return data;
	}

	public boolean updateData(String id, String date, String height, String weight) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL1,id);
		contentValues.put(COL2,date);
		contentValues.put(COL3,height);
		contentValues.put(COL4,weight);
		db.update(TABLE_NAME, contentValues, "ID = ?", new String[] {id});
		return true;
	}

	public Integer deleteData(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		return db.delete(TABLE_NAME, "ID = ?", new String[] {id});
	}
}
