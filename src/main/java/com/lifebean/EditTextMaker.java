package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.util.DisplayMetrics;
import android.graphics.Color;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class EditTextMaker
{
	int screenHeight = 0;
	int screenWidth = 0;
	Context con;

	public EditTextMaker(Context ctx) {
		con = ctx;
		DisplayMetrics dispMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenWidth = dispMetrics.heightPixels;
		screenHeight = dispMetrics.widthPixels;
		System.out.println(screenWidth);
		System.out.println((int)(screenWidth * 0.15));
	}

	public EditText createEditText(String hint) {
		EditText et = new EditText(con);
		LinearLayout.LayoutParams edittext = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		et.setLayoutParams(edittext);
		et.setTextSize(15);
		et.setHintTextColor(Color.GRAY);
		et.setTextColor(Color.parseColor("#000000"));
		et.setHint(hint);
		return(et);
	}
}
