package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.graphics.Color;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.widget.TextView;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.Button;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Gravity;
import android.widget.ScrollView;

public class WalletAddBudgetScreen extends AppCompatActivity
{
	int screenHeight = 0;
	int screenWidth = 0;
	Date date = Calendar.getInstance().getTime();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout layout = new LinearLayout(this);
		TextViewMaker tvm = new TextViewMaker(this);
		EditTextMaker etm = new EditTextMaker(this);
		ButtonMaker btm = new ButtonMaker(this);
		DisplayMetrics dispMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenWidth = dispMetrics.heightPixels;
		screenHeight = dispMetrics.widthPixels;
		layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		layout.setOrientation(LinearLayout.VERTICAL);
		LayoutInflater inflater = LayoutInflater.from(this);
		ScrollView scroll = new ScrollView(this);
		Toolbar toolbar = (Toolbar)inflater.inflate(R.layout.toolbar_layout, null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Add Budget Plan");
		toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		/*Add Budget TextView*/
		LinearLayout screenaddbudget = new LinearLayout(this);
		LinearLayout.LayoutParams addwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		screenaddbudget.setLayoutParams(addwrap);
		addwrap.setMargins(0,50,0,100);
		screenaddbudget.setGravity(Gravity.CENTER);
		LinearLayout addlayout = new LinearLayout(this);
		addlayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		TextView addbudget = tvm.createTextView("Add Budget Plan");
		addbudget.setTextSize(25);
		addlayout.addView(addbudget);
		screenaddbudget.addView(addlayout);
		/*Current Date TextView*/
		LinearLayout screendate = new LinearLayout(this);
		LinearLayout.LayoutParams datewrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		datewrap.setMargins(30,0,0,0);
		TextView currentdate = tvm.createTextView(createDate());
		currentdate.setTextSize(20);
		screendate.setLayoutParams(datewrap);
		screendate.addView(currentdate);
		/*Item EditText*/
		double textwidth = screenHeight * 0.8;
		double textheight = screenWidth * 0.06;
		LinearLayout screenitem = new LinearLayout(this);
		LinearLayout.LayoutParams itemparams = new LinearLayout.LayoutParams((int)textwidth,(int)textheight);
		itemparams.gravity = Gravity.CENTER;
		itemparams.setMargins(0, 50, 0, 30);
		screenitem.setLayoutParams(itemparams);
		EditText item = etm.createEditText("Item");
		screenitem.addView(item);
		/*Price EditText*/
		LinearLayout screenprice = new LinearLayout(this);
		LinearLayout.LayoutParams priceparams = new LinearLayout.LayoutParams((int)textwidth,(int)textheight);
		priceparams.gravity = Gravity.CENTER;
		priceparams.setMargins(0, 20, 0, 30);
		screenprice.setLayoutParams(priceparams);
		EditText price = etm.createEditText("Price");
		screenprice.addView(price);
		/*Button Add*/
		double buttonlimit = screenHeight * 0.2;
		LinearLayout screenbutton = new LinearLayout(this);
		screenbutton.setGravity(Gravity.BOTTOM);
		LinearLayout.LayoutParams forscreenbutton = new LinearLayout.LayoutParams((int)buttonlimit, LinearLayout.LayoutParams.MATCH_PARENT);
		forscreenbutton.setMargins(0,0,0,50);
		forscreenbutton.gravity = Gravity.CENTER;
		screenbutton.setLayoutParams(forscreenbutton);
		RelativeLayout rl = new RelativeLayout(this);
		Button adde = btm.createButton("Add");
		adde.setTextSize(15);
		adde.setWidth((int)(screenWidth * 0.2));
		rl.addView(adde);
		screenbutton.addView(rl);
		layout.addView(toolbar);
		layout.addView(screenaddbudget);
		layout.addView(screendate);
		layout.addView(screenitem);
		layout.addView(screenprice);
		layout.addView(screenbutton);
		scroll.addView(layout);
		setContentView(scroll);
	}

	public String createDate() {
		SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
		String formattedDate = df.format(date);
		return(formattedDate);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
