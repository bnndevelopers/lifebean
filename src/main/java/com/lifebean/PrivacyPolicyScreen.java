package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ScrollView;

public class PrivacyPolicyScreen extends AppCompatActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int screenHeight = 0;
		int screenWidth = 0;
		DisplayMetrics dispMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenHeight = dispMetrics.heightPixels;
		screenWidth = dispMetrics.widthPixels;
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		mainLayout.setGravity(Gravity.CENTER);
		LayoutInflater inflater = LayoutInflater.from(this);
		Toolbar toolbar = (Toolbar)inflater.inflate(R.layout.toolbar_layout, null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Privacy Policy");
		toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		ScrollView scroll = new ScrollView(this);
		scroll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
		LinearLayout centeredLayout = new LinearLayout(this);
		centeredLayout.setGravity(Gravity.CENTER);
		centeredLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
		LinearLayout center = new LinearLayout(this);
		center.setOrientation(LinearLayout.VERTICAL);
		TextView title = new TextView(this);
		title.setGravity(Gravity.CENTER);
		title.setText(R.string.privacy_policy);
		title.setTextSize((float)22.00);
		title.setPadding(30, 30, 30, 30);
		title.setWidth((int)(screenWidth * .80));
		center.addView(title);
		TextView privacyPolicyContentpt1 = new TextView(this);
		privacyPolicyContentpt1.setText(R.string.privacyPolicyContentpt1);
		privacyPolicyContentpt1.setTextSize((float)18.00);
		privacyPolicyContentpt1.setPadding(30, 30, 30, 30);
		privacyPolicyContentpt1.setWidth((int)(screenWidth * .80));
		center.addView(privacyPolicyContentpt1);
		TextView pointOne = new TextView(this);
		pointOne.setText(R.string.point_1);
		pointOne.setTextSize((float)18.00);
		pointOne.setPadding(30, 30, 30, 30);
		pointOne.setWidth((int)(screenWidth * .80));
		center.addView(pointOne);
		TextView pointTwo = new TextView(this);
		pointTwo.setText(R.string.point_2);
		pointTwo.setTextSize((float)18.00);
		pointTwo.setPadding(30, 30, 30, 30);
		pointTwo.setWidth((int)(screenWidth * .80));
		center.addView(pointTwo);
		TextView privacyPolicyContent = new TextView(this);
		privacyPolicyContent.setText(R.string.privacyPolicyContent);
		privacyPolicyContent.setTextSize((float)18.00);
		privacyPolicyContent.setPadding(30, 30, 30, 30);
		privacyPolicyContent.setWidth((int)(screenWidth * .80));
		center.addView(privacyPolicyContent);
		center.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		center.setGravity(Gravity.CENTER);
		centeredLayout.addView(center);
		scroll.addView(centeredLayout);
		mainLayout.addView(toolbar);
		mainLayout.addView(scroll);
		setContentView(mainLayout);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
