package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.view.View;
import android.widget.EditText;
import android.view.Gravity;
import android.graphics.Color;
import android.widget.ImageView;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.widget.TextView;
import android.widget.Button;
import android.util.DisplayMetrics;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.database.Cursor;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.view.View.OnClickListener;
import android.widget.Toast;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.content.Intent;
import android.widget.ScrollView;

public class WalletAddExpenseScreen extends AppCompatActivity
{
	Date date = Calendar.getInstance().getTime();
	int screenHeight = 0;
	int screenWidth = 0;
	private AddExpenseDatabaseHelper addexpense;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addexpense = new AddExpenseDatabaseHelper(this);
		TextViewMaker tvm = new TextViewMaker(this);
		EditTextMaker etm = new EditTextMaker(this);
		ButtonMaker btm = new ButtonMaker(this);
		LinearLayout layout = new LinearLayout(this);
		DisplayMetrics dispMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenWidth = dispMetrics.heightPixels;
		screenHeight = dispMetrics.widthPixels;
		layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		layout.setOrientation(LinearLayout.VERTICAL);
		ScrollView scroll = new ScrollView(this);
		LayoutInflater inflater = LayoutInflater.from(this);
		Toolbar toolbar = (Toolbar)inflater.inflate(R.layout.toolbar_layout, null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Add Expense");
		toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		/*Current Month TextView*/
		LinearLayout screenmonth = new LinearLayout(this);
		LinearLayout.LayoutParams monthparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		screenmonth.setLayoutParams(monthparams);
		monthparams.setMargins(0,150,0,0);
		screenmonth.setGravity(Gravity.CENTER);
		LinearLayout monthlayout = new LinearLayout(this);
		monthlayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		TextView month = tvm.createTextView(createMonth());
		month.setTextSize(20);
		monthlayout.addView(month);
		screenmonth.addView(monthlayout);
		/*Current Date TextView*/
		LinearLayout screendate = new LinearLayout(this);
		screendate.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		screendate.setGravity(Gravity.CENTER);
		LinearLayout datelayout = new LinearLayout(this);
		datelayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		TextView date = tvm.createTextView(createDate());
		date.setTextSize(150);
		datelayout.addView(date);
		screendate.addView(datelayout);
		/*Item EditText*/
		double txtlimit = screenHeight * 0.8;
		double txtwidth = screenWidth * 0.06;
		LinearLayout screenitem = new LinearLayout(this);
		LinearLayout.LayoutParams foritemtxt = new LinearLayout.LayoutParams((int)txtlimit,(int)txtwidth);
		foritemtxt.setMargins(0,0,0,50);
		foritemtxt.gravity = Gravity.CENTER;
		screenitem.setLayoutParams(foritemtxt);
		RelativeLayout rlt = new RelativeLayout(this);
		final EditText item = etm.createEditText("Item");
		item.setTextSize(15);
		rlt.addView(item);
		screenitem.addView(rlt);
		/*Price EditText*/
		LinearLayout screenprice = new LinearLayout(this);
		LinearLayout.LayoutParams forpricetxt = new LinearLayout.LayoutParams((int)txtlimit,(int)txtwidth);
		forpricetxt.setMargins(0,0,0,50);
		forpricetxt.gravity = Gravity.CENTER;
		screenprice.setLayoutParams(forpricetxt);
		RelativeLayout rlp = new RelativeLayout(this);
		final EditText price = etm.createEditText("Price");
		price.setTextSize(15);
		rlp.addView(price);
		screenprice.addView(rlp);
		/*Add and Cancel Buttons*/
		double limit = dispMetrics.widthPixels * 0.8;
		LinearLayout screenbutton = new LinearLayout(this);
		screenbutton.setGravity(Gravity.BOTTOM);
		LinearLayout.LayoutParams forscreenbutton = new LinearLayout.LayoutParams((int)limit, LinearLayout.LayoutParams.WRAP_CONTENT);
		forscreenbutton.setMargins(0,0,0,50);
		forscreenbutton.gravity = Gravity.CENTER;
		screenbutton.setLayoutParams(forscreenbutton);
		RelativeLayout rl = new RelativeLayout(this);
		Button adde = btm.createButton("Add");
		adde.setTextSize(15);
		adde.setWidth((int)(screenWidth * 0.2));
		adde.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				System.out.println("Clicked");
				try {
					String eitem = item.getText().toString();
					int eprice  = Integer.parseInt(price.getText().toString());
					boolean insertData = addexpense.addData(eitem, eprice);
					if(insertData == false) {
						Toast.makeText(WalletAddExpenseScreen.this, "Saved", Toast.LENGTH_LONG).show();
					}
				}
				catch(Exception e) {
				Toast.makeText(WalletAddExpenseScreen.this, "Invalid input", Toast.LENGTH_LONG).show();
				}
			}
		});
		rl.addView(adde);
		LayoutParams addparams = (LayoutParams)adde.getLayoutParams();
		addparams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		Button cancel = btm.createButton("Cancel");
		cancel.setTextSize(15);
		cancel.setWidth((int)(screenWidth * 0.2));
		rl.addView(cancel);
		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});
		LayoutParams cancelparams = (LayoutParams)cancel.getLayoutParams();
		cancelparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		rl.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		screenbutton.addView(rl);
		layout.addView(toolbar);
		layout.addView(screenmonth);
		layout.addView(screendate);
		layout.addView(screenitem);
		layout.addView(screenprice);
		layout.addView(screenbutton);
		scroll.addView(layout);
		setContentView(scroll);
	}

	public String createMonth() {
		SimpleDateFormat df = new SimpleDateFormat("MMMM");
		String formattedDate = df.format(date);
		return(formattedDate);
	}

	public String createDate() {
		SimpleDateFormat df = new SimpleDateFormat("dd");
		String formattedDate = df.format(date);
		return(formattedDate);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
