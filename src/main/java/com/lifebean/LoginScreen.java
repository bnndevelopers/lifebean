package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Button;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.content.Intent;
import android.text.InputType;
import android.widget.Toast;

public class LoginScreen extends Activity
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		EditTextMaker etm = new EditTextMaker(this);
		LinkMaker lmk = new LinkMaker(this);
		int screenHeight = 0;
		int screenWidth = 0;
		DisplayMetrics dispMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenHeight = dispMetrics.heightPixels;
		screenWidth = dispMetrics.widthPixels;

		RelativeLayout mainLayout = new RelativeLayout(this);
		mainLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		mainLayout.setBackgroundColor(Color.parseColor("#CCCC99"));

		ScrollView scroll = new ScrollView(this);
		scroll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));

		LinearLayout centeredLayout = new LinearLayout(this);
		centeredLayout.setOrientation(LinearLayout.VERTICAL);
		centeredLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
		centeredLayout.setGravity(Gravity.CENTER);

		LinearLayout center = new LinearLayout(this);
		center.setOrientation(LinearLayout.VERTICAL);
		ImageView charLogo = new ImageView(this);
		charLogo.setBackgroundResource(R.drawable.lifebean_logo);
		charLogo.setLayoutParams(new LinearLayout.LayoutParams((int)(screenWidth * .60), (int)(screenHeight * .30)));
		centeredLayout.addView(charLogo);
		final EditText username = etm.createEditText("Username");
		username.setTextSize((float)18.00);
		username.setPadding(30, 30, 30, 30);
		username.setWidth((int)(screenWidth * .60));
		center.addView(username);
		final EditText password = etm.createEditText("Password");
		password.setTextSize((float)18.00);
		password.setPadding(30, (int)(screenHeight * .05), 30, 30);
		password.setWidth((int)(screenWidth * .60));
		center.addView(password);
		password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		password.setSelection(password.getText().length());
		TextView forgotpw = lmk.createLink("Forgot Password");
		forgotpw.setPadding(30, 30, 30, 70);
		center.addView(forgotpw);
		Button login = new Button(this);
		login.setText("Login");
		login.setTextColor(Color.parseColor("#FFFFFF"));
		login.setBackgroundColor(Color.parseColor("#669900"));
		login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				System.out.println("Clicked");
				String[] credentials = {username.getText().toString(), password.getText().toString()};
				new ServerTask(ResponseGetter.lifebeanUrl + "login").execute(credentials);
				if(ServerTask.isAccessed) {
					startActivity(new Intent(getApplicationContext(), MainMenuScreen.class));
				}
			}
		});
		center.addView(login);
		center.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		center.setGravity(Gravity.CENTER);
		center.setBackgroundColor(Color.parseColor("#FFFFFF"));
		center.setPadding((int)(screenWidth * .10), (int)(screenHeight * .10), (int)(screenWidth * .10), (int)(screenHeight * .10));
		centeredLayout.addView(center);
		scroll.addView(centeredLayout);
		mainLayout.addView(scroll);
		setContentView(mainLayout);
	}
}
