package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;

public class AddExpenseDatabaseHelper extends SQLiteOpenHelper
{
	public static final String DATABASE_NAME = "addexpense.db";
	public static final String TABLE_NAME = "addexpense_tbl";
	public static final String COL1 = "Item";
	public static final String COL2 = "Price";

	public AddExpenseDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String createTable = "CREATE TABLE " + TABLE_NAME + " (Item TEXT, " + " PRICE INTEGER)";
		db.execSQL(createTable);
		System.out.println("Database created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP IF TABLE EXISTS " + TABLE_NAME);
		onCreate(db);
	}

	public boolean addData(String item, int price) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL1,item);
		contentValues.put(COL2,price);

		long result = db.insert(TABLE_NAME, null, contentValues);
		
		if(result == 1) {
			return true;
		}
		else {
			return false;
		}
	}

	public Cursor showData() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor data = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "'", null);
		return data;
	}

	public boolean updateData(String item, int price) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL1,item);
		contentValues.put(COL2,price);
		db.update(TABLE_NAME, contentValues, "Item = ?", new String[] {item});
		return true;
	}
}
