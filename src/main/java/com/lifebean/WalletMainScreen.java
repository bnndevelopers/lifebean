package com.lifebean;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.view.View;
import android.widget.Button;
import android.view.Gravity;
import android.graphics.Color;
import android.widget.ImageView;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.widget.TextView;
import android.content.Intent;
import android.view.LayoutInflater;
import android.widget.ScrollView;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.util.DisplayMetrics;

public class WalletMainScreen extends AppCompatActivity
{
	int screenHeight = 0;
	int screenWidth = 0;
	Date date = Calendar.getInstance().getTime();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ButtonMaker btm = new ButtonMaker(this);
		TextViewMaker tvm = new TextViewMaker(this);
		LinearLayout layout = new LinearLayout(this);
		DisplayMetrics dispMetrics = new DisplayMetrics();
		AddExpenseDatabaseHelper aedh = new AddExpenseDatabaseHelper(this);
		(this).getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenWidth = dispMetrics.heightPixels;
		layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		layout.setOrientation(LinearLayout.VERTICAL);
		ScrollView scroll = new ScrollView(this);
		LayoutInflater inflater = LayoutInflater.from(this);
		Toolbar toolbar = (Toolbar)inflater.inflate(R.layout.toolbar_layout, null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Wallet");
		toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		layout.addView(toolbar);
		/*IMAGE LAYOUT*/
		LinearLayout screenlayout = new LinearLayout(this);
		screenlayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		LinearLayout image = new LinearLayout(this);
		LinearLayout.LayoutParams imagewrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		image.setLayoutParams(imagewrap);
		image.addView(createImage());
		screenlayout.addView(image);
		/*DATE LAYOUT*/
		LinearLayout screendate = new LinearLayout(this);
		LinearLayout.LayoutParams datewrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		datewrap.setMargins(30,0,0,0);
		TextView currentdate = tvm.createTextView(createDate());
		currentdate.setTextSize(15);
		screendate.setLayoutParams(datewrap);
		screendate.addView(currentdate);
		/*DAY LAYOUT*/
		LinearLayout screenday = new LinearLayout(this);
		LinearLayout.LayoutParams daywrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		daywrap.setMargins(30,0,0,10);
		screenday.setLayoutParams(daywrap);
		int dday = date.getDay();
		TextView currentday = tvm.createTextView(getDay(dday));
		currentday.setTextSize(15);
		screenday.addView(currentday);
		/*CURRENT EXPENSES TEXTVIEW*/
		LinearLayout screenexpenses = new LinearLayout(this);
		LinearLayout.LayoutParams currentwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		screenexpenses.setLayoutParams(currentwrap);
		currentwrap.setMargins(30,0,0,10);
		TextView current = tvm.createTextView("Current Expenses: ");
		current.setTextSize(15);
		screenexpenses.addView(current);
		/*CURRENT EXPENSES TOTAL TEXTVIEW*/
		// LinearLayout screentotal = new LinearLayout(this);
		// LinearLayout.LayoutParams totalwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		// screentotal.setLayoutParams(totalwrap);
		// totalwrap.setMargins(30,0,0,10);
		// TextView total = tvm.createTextView(aedh.showData().toString());
		// total.setTextSize(15);
		// screentotal.addView(total);
		/*BUTTON SCREEN LAYOUT*/
		LinearLayout screenbutton = new LinearLayout(this);
		screenbutton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		screenbutton.setGravity(Gravity.BOTTOM);
		LinearLayout btnlayout = new LinearLayout(this);
		btnlayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		Button add = btm.createButton("Add Expense");
		LinearLayout addlayout = new LinearLayout(this);
		LinearLayout.LayoutParams addparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		addparams.setMargins(30, 0, 15, 50);
		addlayout.setLayoutParams(addparams);
		add.setWidth((int)(screenWidth * 0.15));
		add.setTextSize(12);
		addlayout.addView(add);
		btnlayout.addView(addlayout);
		add.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), WalletAddExpenseScreen.class);
				startActivity(intent);
			}
		});
		Button daily = btm.createButton("Daily Expense");
		LinearLayout dailylayout = new LinearLayout(this);
		LinearLayout.LayoutParams dailyparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		dailyparams.setMargins(15, 0, 15, 50);
		dailylayout.setLayoutParams(dailyparams);
		daily.setWidth((int)(screenWidth * 0.15));
		daily.setTextSize(12);
		btnlayout.addView(dailylayout);
		dailylayout.addView(daily);
		daily.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), WalletDailyExpenseScreen.class);
				startActivity(intent);
			}
		});
		Button monthly = btm.createButton("Monthly Expense");
		LinearLayout monthlylayout = new LinearLayout(this);
		LinearLayout.LayoutParams monthparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		monthparams.setMargins(15, 0, 30, 50);
		monthlylayout.setLayoutParams(monthparams);
		monthly.setWidth((int)(screenWidth * 0.2));
		monthly.setTextSize(12);
		monthlylayout.addView(monthly);
		btnlayout.addView(monthlylayout);
		monthly.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				Intent intent = new Intent(getApplicationContext(), WalletMonthExpenseScreen.class);
				startActivity(intent);
			}
		});
		screenbutton.addView(btnlayout);
		layout.addView(screenlayout);
		layout.addView(screendate);
		layout.addView(screenday);
		layout.addView(screenexpenses);
		scroll.addView(layout);
		// layout.addView(screentotal);
		layout.addView(screenbutton);
		setContentView(scroll);
	}

	public ImageView createImage() {
		ImageView icon = new ImageView(this);
		icon.setImageResource(R.drawable.walleticon);
		return(icon);
	}

	public String getDay(int day) {
		String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
		return(days[day]);
	}

	public String createDate() {
		SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
		String formattedDate = df.format(date);
		return(formattedDate);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
