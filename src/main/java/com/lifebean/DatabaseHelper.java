package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;

public class DatabaseHelper extends SQLiteOpenHelper
{
	public static final String DATABASE_NAME = "outrun.db";
	public static final String TABLE_NAME = "outrun_table";
	public static final String COL1 = "ID"; //position 0
	public static final String COL2 = "DATE"; //position 1
	public static final String COL3 = "STARTTIME"; //position 2
	public static final String COL4 = "ENDTIME"; //position 3
	public static final String COL5 = "LOCATION"; //position 5
	public static final String COL6 = "WEIGHT"; //position 6

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String createTable = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " + " DATE TEXT, STARTTIME TEXT, ENDTIME TEXT, LOCATION TEXT, WEIGHT TEXT)";
		db.execSQL(createTable);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP IF TABLE EXISTS " + TABLE_NAME);
		onCreate(db);
	}

	public boolean addData(String date, String starttime, String endtime, String location, String weight) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL2,date);
		contentValues.put(COL3,starttime);
		contentValues.put(COL4,endtime);
		contentValues.put(COL5,location);
		contentValues.put(COL6,weight);

		long result = db.insert(TABLE_NAME, null, contentValues);
		
		if(result == -1) {
			return false;
		}
		else {
			return true;
		}
	}

	public Cursor showData() {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor data = db.rawQuery("SELECT * FROM '" + TABLE_NAME + "'", null);
		return data;
	}

	public boolean updateData(String id, String date, String starttime, String endtime, String location, String weight) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(COL1,id);
		contentValues.put(COL2,date);
		contentValues.put(COL3,starttime);
		contentValues.put(COL4,endtime);
		contentValues.put(COL5,location);
		contentValues.put(COL6,weight);
		db.update(TABLE_NAME, contentValues, "ID = ?", new String[] {id});
		return true;
	}

	public Integer deleteData(String id) {
		SQLiteDatabase db = this.getWritableDatabase();
		return db.delete(TABLE_NAME, "ID = ?", new String[] {id});
	}
}
