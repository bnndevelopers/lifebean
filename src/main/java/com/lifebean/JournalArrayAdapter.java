package com.lifebean;

import android.widget.ArrayAdapter;
import java.util.ArrayList;
import java.util.List;
import android.widget.TextView;
import android.view.View;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

public class JournalArrayAdapter extends ArrayAdapter<JournalItem>
{
	Context con = null;
	List<JournalItem> items = null;

	public JournalArrayAdapter(Context context, ArrayList<JournalItem> items) {
		super(context, 0, items);
		con = context;
		this.items = items;
	} 

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		JournalItem jitem = items.get(position);
		TextView vv = null;
		if (item == null) {
			if("task".equals(jitem.getType())) {
				item = LayoutInflater.from(con).inflate(R.layout.task_listviewitem, parent, false);
			}
			else {
				item = LayoutInflater.from(con).inflate(R.layout.note_event_listviewitem, parent, false);
			}
		}
		vv = (TextView)item.findViewById(R.id.listviewItem);
		vv.setText(jitem.getTitle());
		return item;
	}
}
