package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.view.Gravity;
import android.widget.LinearLayout.LayoutParams;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class AboutScreen extends AppCompatActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int screenHeight = 0;
		int screenWidth = 0;
		DisplayMetrics dispMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenHeight = dispMetrics.heightPixels;
		screenWidth = dispMetrics.widthPixels;
		LinearLayout mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		LayoutInflater inflater = LayoutInflater.from(this);
		Toolbar toolbar = (Toolbar)inflater.inflate(R.layout.toolbar_layout, null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle("About LifeBean");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		LinearLayout centeredLayout = new LinearLayout(this);
		centeredLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		LinearLayout center = new LinearLayout(this);
		TextView title = new TextView(this);
		title.setGravity(Gravity.CENTER);
		title.setText(R.string.about);
		title.setTextSize((float)18.00);
		title.setPadding(30, 30, 30, 30);
		title.setWidth((int)(screenWidth * .80));
		center.addView(title);
		TextView about = new TextView(this);
		about.setText(R.string.about_pt_1);
		about.setTextSize((float)18.00);
		about.setPadding(30, 30, 30, 30);
		about.setWidth((int)(screenWidth * .80));
		center.addView(about);
		center.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		center.setGravity(Gravity.CENTER);
		center.setOrientation(LinearLayout.VERTICAL);
		centeredLayout.setGravity(Gravity.CENTER);
		centeredLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
		centeredLayout.addView(center);
		mainLayout.addView(toolbar);
		mainLayout.addView(centeredLayout);
		setContentView(mainLayout);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
