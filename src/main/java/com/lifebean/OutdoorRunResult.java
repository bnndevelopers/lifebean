package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.View;
import android.graphics.Color;
import android.widget.TextView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class OutdoorRunResult extends AppCompatActivity 
{
	String res;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resultor_main);
		Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Outdoor Run Result");
		res = getIntent().getExtras().getString("RESULT");
		TextView textView1 =  (TextView)findViewById(R.id.rr);
		textView1.setGravity(Gravity.CENTER);
		textView1.setText("Excellent!"+ "\n" + "Burned Calories" + "\n" + res);
		TextView textView2 = (TextView)findViewById(R.id.tvResultFooter);
		textView2.setGravity(Gravity.CENTER);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
