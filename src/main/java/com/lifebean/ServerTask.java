package com.lifebean;

import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedOutputStream;
import org.json.JSONObject;
import android.util.JsonWriter;

public class ServerTask extends AsyncTask<String, Void, String>
{
	String serverUrl;
	static boolean isAccessed;

	public ServerTask(String u) {
		serverUrl = u;
	}

	protected String doInBackground(String... str) {
		String response = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(serverUrl);
			urlConnection = (HttpURLConnection)url.openConnection();
			urlConnection.setDoOutput(true);
			urlConnection.setChunkedStreamingMode(0);
			OutputStream ostr = new BufferedOutputStream(urlConnection.getOutputStream());
			JsonWriter jwriter = new JsonWriter(new OutputStreamWriter(ostr, "UTF-8"));
			jwriter.beginObject();
			jwriter.name("un").value(str[0]);
			jwriter.name("pw").value(str[1]);
			jwriter.endObject();
			jwriter.close();
			response = ResponseGetter.getResponse(urlConnection);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			if(urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return(response);
	}

	protected void onPostExecute(String result) {
		System.out.println("Response: " + result + " please.");
		try {
			JSONObject jObj = new JSONObject(result);
			System.out.println("True " + (String)jObj.get("result"));
			switch((String)jObj.get("result")) {
				case "valid" : {
					System.out.println("Login success");
					isAccessed = true;
					break;
				}
				case "invalid" : {
					System.out.println("Login failed");
					isAccessed = false;
					break;
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
