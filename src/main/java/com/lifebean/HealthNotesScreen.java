package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.graphics.Color;
import android.widget.DatePicker;
import java.util.Calendar;
import android.app.DatePickerDialog;
import android.graphics.drawable.ColorDrawable;
import android.widget.EditText;
import android.view.Gravity;
import android.widget.Toast;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.database.Cursor;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.LayoutInflater;

public class HealthNotesScreen extends AppCompatActivity
{
	private EditText et;
	private Button btnSaveNotes;
	private Button btnViewNotes;
	private Button btnUpdate;
	private Button btnDelete;
	private HNotesDatabaseHelper hnotesDB;
	private TextView mDisplayDate;
	private DatePickerDialog.OnDateSetListener mDateSetListener;
	private Calendar calendar;
	private EditText etID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hnotes_main);
		Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Health Note");
		hnotesDB = new HNotesDatabaseHelper(this);
		et = (EditText)findViewById(R.id.et1);
		btnSaveNotes = (Button) findViewById(R.id.btnSaveNotes);
		btnViewNotes = (Button) findViewById(R.id.btnViewNotes);
		mDisplayDate = (TextView)findViewById(R.id.tvcal);
		btnUpdate = (Button) findViewById(R.id.btnUpdate);
		btnDelete = (Button) findViewById(R.id.btnDel);
		etID = (EditText)findViewById(R.id.etID);
		mDisplayDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar cal = Calendar.getInstance();
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				DatePickerDialog dialog = new DatePickerDialog(HealthNotesScreen.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,mDateSetListener,year,month,day);
				dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				dialog.show();
			}
		});
		mDateSetListener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker datePicker, int year, int month, int day) {
				month = month + 1;
				String date = month + "/" + day + "/" + year;
				mDisplayDate.setText(date);
			}
		};
		saveData();
		viewHealthNotes();
		updateData();
		DeleteData();
	}

	public void saveData() {
		btnSaveNotes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String dt = mDisplayDate.getText().toString();
				String nt = et.getText().toString();
				if (dt != null && !"".equals(dt) && nt != null && !"".equals(nt)) {
					boolean insertData = hnotesDB.addData(dt, nt);
					if(insertData == true) {
						Toast.makeText(HealthNotesScreen.this, "Health Notes Saved", Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(HealthNotesScreen.this, "Something went wrong", Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(HealthNotesScreen.this, "Please complete the details", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

		public void viewHealthNotes() {
		btnViewNotes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Cursor data = hnotesDB.showData();
				if(data.getCount() == 0) {
					display("Error", "No data found.");
					return;
				}
				StringBuffer buffer = new StringBuffer();
				while(data.moveToNext()) {
					buffer.append("ID: " + data.getString(0) + "\n");
					buffer.append("Date: " + data.getString(1) + "\n");
					buffer.append("Health Notes: " + data.getString(2) + "\n");
				}
				display("HEALTH NOTES RECORDS:", buffer.toString());
			}
		});
	}

	public void display(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}

	public void updateData() {
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = etID.getText().toString().length();
				if(temp > 0) {
					boolean update = hnotesDB.updateData(etID.getText().toString(),mDisplayDate.getText().toString(),et.getText().toString());
					if(update == true) {
						Toast.makeText(HealthNotesScreen.this, "Health notes updated",Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(HealthNotesScreen.this, "Something went wrong",Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(HealthNotesScreen.this, "Enter an ID to update",Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void DeleteData() {
		btnDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = etID.getText().toString().length();
				if(temp > 0) {
					Integer deleteRow = hnotesDB.deleteData(etID.getText().toString());
					if(deleteRow > 0) {
						Toast.makeText(HealthNotesScreen.this,"Health notes deleted",Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(HealthNotesScreen.this,"Something went wrong",Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(HealthNotesScreen.this, "You must enter an ID to delete", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
