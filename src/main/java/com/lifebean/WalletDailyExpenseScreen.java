package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.graphics.Color;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.widget.TextView;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

public class WalletDailyExpenseScreen extends AppCompatActivity
{
	int screenHeight = 0;
	int screenWidth = 0;
	Date date = Calendar.getInstance().getTime();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout layout = new LinearLayout(this);
		LayoutInflater inflater = LayoutInflater.from(this);
		ScrollView scroll = new ScrollView(this);
		Toolbar toolbar = (Toolbar)inflater.inflate(R.layout.toolbar_layout, null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Daily Expense");
		toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		TextViewMaker tvm = new TextViewMaker(this);
		layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		layout.setOrientation(LinearLayout.VERTICAL);
		/*Current Date TextView*/
		LinearLayout screendate = new LinearLayout(this);
		LinearLayout.LayoutParams datewrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		datewrap.setMargins(30,0,0,0);
		TextView currentdate = tvm.createTextView(createDate());
		currentdate.setTextSize(20);
		screendate.setLayoutParams(datewrap);
		screendate.addView(currentdate);
		/*DAY LAYOUT*/
		LinearLayout screenday = new LinearLayout(this);
		LinearLayout.LayoutParams daywrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		daywrap.setMargins(30,0,0,20);
		int dday = date.getDay();
		TextView currentday = tvm.createTextView(getDay(dday));
		currentday.setTextSize(20);
		screenday.setLayoutParams(daywrap);
		screenday.addView(currentday);
		/*Daily Expense Summary ListView TextView*/
		LinearLayout screensum = new LinearLayout(this);
		LinearLayout.LayoutParams sumwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		screensum.setLayoutParams(sumwrap);
		sumwrap.setMargins(0,50,0,100);
		screensum.setGravity(Gravity.CENTER);
		LinearLayout sumlayout = new LinearLayout(this);
		sumlayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		TextView summary = tvm.createTextView("Daily Expenses Summary");
		summary.setTextSize(15);
		sumlayout.addView(summary);
		screensum.addView(sumlayout);
		/*Current Expense Text*/
		LinearLayout screenexpenses = new LinearLayout(this);
		LinearLayout.LayoutParams currentwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		screenexpenses.setLayoutParams(currentwrap);
		currentwrap.setMargins(30,0,0,10);
		TextView current = tvm.createTextView("Current Expenses: ");
		current.setTextSize(15);
		screenexpenses.addView(current);
		/*Current Expenses Total Text*/
		LinearLayout screentotal = new LinearLayout(this);
		LinearLayout.LayoutParams totalwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		screentotal.setLayoutParams(totalwrap);
		totalwrap.setMargins(30,0,0,10);
		TextView total = tvm.createTextView("Total");
		total.setTextSize(15);
		screentotal.addView(total);
		/*ImageView*/
		LinearLayout screenimage = new LinearLayout(this);
		screenimage.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		LinearLayout image = new LinearLayout(this);
		LinearLayout.LayoutParams imagewrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		image.setLayoutParams(imagewrap);
		image.addView(createImage());
		image.setPadding(100,100,0,0);
		RelativeLayout rl = new RelativeLayout(this);
		rl.addView(image);
		LayoutParams imageparams = (LayoutParams)image.getLayoutParams();
		imageparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		screenimage.addView(rl);
		layout.addView(toolbar);
		layout.addView(screendate);
		layout.addView(screenday);
		layout.addView(screensum);
		layout.addView(screenexpenses);
		layout.addView(screentotal);
		layout.addView(screenimage);
		scroll.addView(layout);
		setContentView(scroll);
	}

	public String getDay(int day) {
		String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
		return(days[day]);
	}

	public String createDate() {
		SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
		String formattedDate = df.format(date);
		return(formattedDate);
	}

	public ImageView createImage() {
		ImageView icon = new ImageView(this);
		icon.setImageResource(R.drawable.pig);
		return(icon);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
