package com.lifebean;

import android.graphics.Paint;
import android.widget.TextView;
import android.graphics.Color;
import android.content.Context;

public class LinkMaker
{
	Context con;

	public LinkMaker(Context ctx) {
		con = ctx;
	}

	public TextView createLink(String text) {
		TextView tv = new TextView(con);
		tv.setText(text);
		tv.setTextSize(15);
		tv.setTextColor(Color.parseColor("#669900"));
		tv.setPaintFlags(tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		return(tv);
	}
}
