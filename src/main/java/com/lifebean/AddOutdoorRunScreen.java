package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Button;
import android.content.Intent;
import android.view.View;
import android.app.DatePickerDialog;
import android.widget.TextView;
import java.util.Calendar;
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
import android.widget.DatePicker;
import android.widget.EditText;
import android.app.TimePickerDialog;
import android.widget.TimePicker;
import android.view.View.OnClickListener;
import android.widget.Toast;
import android.database.Cursor;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.NumberFormat;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.LayoutInflater;
import android.text.Editable;
import android.text.TextWatcher;

public class AddOutdoorRunScreen extends AppCompatActivity
{
	private TextView mDisplayDate;
	private DatePickerDialog.OnDateSetListener mDateSetListener;
	private EditText chooseTime;
	private EditText chooseEndTime;
	private TimePickerDialog timePickerDialog;
	private Calendar calendar;
	private int currentHour;
	private int currentMinute;
	private String amPm;
	private EditText etLocation;
	private EditText etWeight;
	private Button btnAddData;
	private Button btnViewData;
	private ORDatabaseHelper outrunDB;
	private Button btnUpdate;
	private EditText etID;
	private Button btnDelete;
	private String time1;
	private String time2;
	private SimpleDateFormat sdf;
	private Date parseTime1;
	private Date parseTime2;
	private Double hours;
	private TextView tvShowcal;
	private double weight2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addoutrun_main);
		Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("New Outdoor Run");
		mDisplayDate = (TextView)findViewById(R.id.tvDateOutdoorRun);
		mDisplayDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar cal = Calendar.getInstance();
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				DatePickerDialog dialog = new DatePickerDialog(AddOutdoorRunScreen.this,android.R.style.Theme_Holo_Light_Dialog_MinWidth,mDateSetListener,year,month,day);
				dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				dialog.show();
			}
		});

		mDateSetListener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker datePicker, int year, int month, int day) {
				month = month + 1;
				String date = month + "/" + day + "/" + year;
				mDisplayDate.setText(date);
			}
		};

		chooseTime = (EditText)findViewById(R.id.etChooseTime);
		chooseTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				calendar = Calendar.getInstance();
				currentHour = calendar.get(Calendar.HOUR_OF_DAY);
				currentMinute = calendar.get(Calendar.MINUTE);
				timePickerDialog = new TimePickerDialog(AddOutdoorRunScreen.this, new TimePickerDialog.OnTimeSetListener() {
				@Override
				public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
					if(hourOfDay >= 12) {
						amPm = "PM";
					}
					else {
						amPm = "AM";
					}
					chooseTime.setText(String.format("%02d:%02d", hourOfDay, minutes) + amPm);
				}
			}, currentHour, currentMinute, false);
			timePickerDialog.show();
			}
		});

		chooseEndTime = (EditText)findViewById(R.id.etChooseEndTime);
		chooseEndTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				calendar = Calendar.getInstance();
				currentHour = calendar.get(Calendar.HOUR_OF_DAY);
				currentMinute = calendar.get(Calendar.MINUTE);
				timePickerDialog = new TimePickerDialog(AddOutdoorRunScreen.this, new TimePickerDialog.OnTimeSetListener() {
				@Override
				public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
					if(hourOfDay >= 12) {
						amPm = "PM";
					}
					else {
						amPm = "AM";
					}
					chooseEndTime.setText(String.format("%02d:%02d", hourOfDay, minutes) + amPm);
				}
			}, currentHour, currentMinute, false);
			timePickerDialog.show();
			}
		});

		etLocation = (EditText)findViewById(R.id.etLocation);
		btnAddData = (Button)findViewById(R.id.btnAddData);
		etWeight = (EditText)findViewById(R.id.etWeight);
		btnViewData = (Button)findViewById(R.id.btnViewData);
		outrunDB = new ORDatabaseHelper(this);
		btnUpdate = (Button)findViewById(R.id.btnUpdate);
		etID = (EditText)findViewById(R.id.etID);
		btnDelete = (Button)findViewById(R.id.btnDelete);
		tvShowcal = (TextView)findViewById(R.id.tvResult);

		saveData();
		viewRunHistory();
		updateData();
		DeleteData();
	}

	public void computeTime() {
		time1 = chooseTime.getText().toString();
		time2 = chooseEndTime.getText().toString();
		weight2 = Double.valueOf(etWeight.getText().toString());
		sdf = new SimpleDateFormat("HH:mm");
		double hours =0;
		double secs;

		try {
			parseTime1 = sdf.parse(time1);
			parseTime2 = sdf.parse(time2);
			secs = (parseTime2.getTime() - parseTime1.getTime()) / 1000;
			hours = ((secs / 60) * ((weight2 * 8 * 3.5)/200));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		displayDiff(hours);
	}

	public void displayDiff(double hours) {
		String diff = ((new Double(hours)).toString());
		tvShowcal.setText(diff);
		Intent intent = new Intent(AddOutdoorRunScreen.this, OutdoorRunResult.class);
		intent.putExtra("RESULT", diff);
		startActivity(intent);
	}

	public void saveData() {
		btnAddData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				computeTime();
				String date = mDisplayDate.getText().toString();
				String starttime = chooseTime.getText().toString();
				String endtime = chooseEndTime.getText().toString();
				String location = etLocation.getText().toString();
				String weight = etWeight.getText().toString();
				String timediff = tvShowcal.getText().toString();
				boolean insertData = outrunDB.addData(date, starttime, endtime, location, weight,timediff);
				if(insertData == true) {
					Toast.makeText(AddOutdoorRunScreen.this, "Outdoor Run Saved!", Toast.LENGTH_LONG).show();
				}
				else {
					Toast.makeText(AddOutdoorRunScreen.this, "Something went wrong :(", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void viewRunHistory() {
		btnViewData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Cursor data = outrunDB.showData();
				if(data.getCount() == 0) {
					display("Error", "No data found.");
					return;
				}
				StringBuffer buffer = new StringBuffer();
				while(data.moveToNext()) {
					buffer.append("ID: " + data.getString(0) + "\n");
					buffer.append("Date: " + data.getString(1) + "\n");
					buffer.append("Start Time: " + data.getString(2) + "\n");
					buffer.append("End Time: " + data.getString(3) + "\n");
					buffer.append("Location: " + data.getString(4) + "\n");
					buffer.append("Weight(KG): " + data.getString(5) + "\n");
					buffer.append("Calories Burned: " + data.getString(6) + "\n");
				}
				//show all data
				display("All Outdoor Runs:", buffer.toString());
			}
		});
	}

	public void display(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}

	public void updateData() {
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = etID.getText().toString().length();
				if(temp > 0) {
					boolean update = outrunDB.updateData(etID.getText().toString(),mDisplayDate.getText().toString(),chooseTime.getText().toString(),chooseEndTime.getText().toString(),etLocation.getText().toString(),etWeight.getText().toString(),(new Double(hours)).toString());
					if(update == true) {
						Toast.makeText(AddOutdoorRunScreen.this, "Succsessfully updated data.",Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(AddOutdoorRunScreen.this, "Something went wrong! :(",Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(AddOutdoorRunScreen.this, "Enter an ID to update.",Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void DeleteData() {
		btnDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = etID.getText().toString().length();
				if(temp > 0) {
					Integer deleteRow = outrunDB.deleteData(etID.getText().toString());
					if(deleteRow > 0) {
						Toast.makeText(AddOutdoorRunScreen.this,"Successfully DELETED the data",Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(AddOutdoorRunScreen.this,"Something went wrong.",Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(AddOutdoorRunScreen.this, "You must enter an ID to delete :( ", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
