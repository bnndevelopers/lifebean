package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Button;
import android.content.Intent;
import android.view.View;
import android.app.DatePickerDialog;
import android.widget.TextView;
import java.util.Calendar;
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
import android.widget.DatePicker;
import android.widget.EditText;
import android.app.TimePickerDialog;
import android.widget.TimePicker;
import android.view.View.OnClickListener;
import android.widget.Toast;
import android.database.Cursor;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.LayoutInflater;
import android.text.Editable;
import android.text.TextWatcher;

public class AddWorkoutScreen extends AppCompatActivity
{
	private TextView mDisplayDate;
	private DatePickerDialog.OnDateSetListener mDateSetListener;
	private EditText chooseTime;
	private EditText chooseEndTime;
	private TimePickerDialog timePickerDialog;
	private Calendar calendar;
	private int currentHour;
	private int currentMinute;
	private String amPm;
	private EditText etLocation;
	private EditText etDetails;
	private Button btnAddData;
	private Button btnViewData;
	private Button btnUpdate;
	private Button btnDelete;
	private EditText etID;
	private AWDatabaseHelper workoutDB;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addworkout_main);
		Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("New Workout");
		mDisplayDate = (TextView)findViewById(R.id.tvDateWorkout);
		mDisplayDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar cal = Calendar.getInstance();
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				DatePickerDialog dialog = new DatePickerDialog(AddWorkoutScreen.this,android.R.style.Theme_Holo_Light_Dialog_MinWidth,mDateSetListener,year,month,day);
				dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				dialog.show();
			}
		});

		mDateSetListener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker datePicker, int year, int month, int day) {
				month = month + 1;
				String date = month + "/" + day + "/" + year;
				mDisplayDate.setText(date);
			}
		};

		chooseTime = (EditText)findViewById(R.id.etChooseTime);
		chooseTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				calendar = Calendar.getInstance();
				currentHour = calendar.get(Calendar.HOUR_OF_DAY);
				currentMinute = calendar.get(Calendar.MINUTE);
				timePickerDialog = new TimePickerDialog(AddWorkoutScreen.this, new TimePickerDialog.OnTimeSetListener() {
				@Override
				public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
					if(hourOfDay >= 12) {
						amPm = "PM";
					}
					else {
						amPm = "AM";
					}
					chooseTime.setText(String.format("%02d:%02d", hourOfDay, minutes) + amPm);
				}
			}, currentHour, currentMinute, false);

			timePickerDialog.show();
			}
		});

		chooseEndTime = (EditText)findViewById(R.id.etChooseEndTime);
		chooseEndTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				calendar = Calendar.getInstance();
				currentHour = calendar.get(Calendar.HOUR_OF_DAY);
				currentMinute = calendar.get(Calendar.MINUTE);

				timePickerDialog = new TimePickerDialog(AddWorkoutScreen.this, new TimePickerDialog.OnTimeSetListener() {
				@Override
				public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
					if(hourOfDay >= 12) {
						amPm = "PM";
					}
					else {
						amPm = "AM";
					}
					// chooseTime.setText(hourOfDay + ":" + minutes + " " + amPm);
					chooseEndTime.setText(String.format("%02d:%02d", hourOfDay, minutes) + amPm);
				}
			}, currentHour, currentMinute, false);

			timePickerDialog.show();
			}
		});

		etLocation = (EditText)findViewById(R.id.etLocation);
		etDetails = (EditText)findViewById(R.id.etDetails);
		etID = (EditText)findViewById(R.id.etID);
		btnAddData = (Button)findViewById(R.id.btnAddData);
		btnViewData = (Button)findViewById(R.id.btnViewData);
		btnUpdate = (Button)findViewById(R.id.btnUpdate);
		btnDelete = (Button)findViewById(R.id.btnDelete);
		workoutDB = new AWDatabaseHelper(this);

		saveData();
		viewRunHistory();
		updateData();
		DeleteData();

	}

	public void saveData() {
		btnAddData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String date = mDisplayDate.getText().toString();
				String starttime = chooseTime.getText().toString();
				String endtime = chooseEndTime.getText().toString();
				String location = etLocation.getText().toString();
				String details = etDetails.getText().toString();
				boolean insertData = workoutDB.addData(date, starttime, endtime, location, details);
				if(insertData == true) {
					Toast.makeText(AddWorkoutScreen.this, "Workout Saved!", Toast.LENGTH_LONG).show();
				}
				else {
					Toast.makeText(AddWorkoutScreen.this, "Something went wrong :(", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void viewRunHistory() {
		btnViewData.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Cursor data = workoutDB.showData();
				if(data.getCount() == 0) {
					display("Error", "No data found.");
					return;
				}
				StringBuffer buffer = new StringBuffer();
				while(data.moveToNext()) {
					buffer.append("ID: " + data.getString(0) + "\n");
					buffer.append("Date: " + data.getString(1) + "\n");
					buffer.append("Start Time: " + data.getString(2) + "\n");
					buffer.append("End Time: " + data.getString(3) + "\n");
					buffer.append("Gym: " + data.getString(4) + "\n");
					buffer.append("Details: " + data.getString(5) + "\n");
				}
				display("All Workouts:", buffer.toString());
			}
		});
	}

	public void display(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}

	public void updateData() {
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = etID.getText().toString().length();
				if(temp > 0) {
					boolean update = workoutDB.updateData(etID.getText().toString(),mDisplayDate.getText().toString(),chooseTime.getText().toString(),chooseEndTime.getText().toString(),etLocation.getText().toString(),etDetails.getText().toString());
					if(update == true) {
						Toast.makeText(AddWorkoutScreen.this, "Succsessfully updated data.",Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(AddWorkoutScreen.this, "Something went wrong! :(",Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(AddWorkoutScreen.this, "Enter an ID to update.",Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void DeleteData() {
		btnDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = etID.getText().toString().length();
				if(temp > 0) {
					Integer deleteRow = workoutDB.deleteData(etID.getText().toString());
					if(deleteRow > 0) {
						Toast.makeText(AddWorkoutScreen.this,"Successfully DELETED the data",Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(AddWorkoutScreen.this,"Something went wrong.",Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(AddWorkoutScreen.this, "You must enter an ID to delete :( ", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
