package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.util.DisplayMetrics;
import android.graphics.Color;
import android.content.Context;
import android.widget.LinearLayout;

public class TextViewMaker
{
	int screenHeight;
	int screenWidth;
	Context con;

	public TextViewMaker(Context ctx) {
		con = ctx;
		DisplayMetrics dispMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(dispMetrics);
		screenWidth = dispMetrics.heightPixels;
		screenHeight = dispMetrics.widthPixels;
	}

	public TextView createTextView(String settext) {
		TextView tv = new TextView(con);
		tv.setTextColor(Color.BLACK);
		tv.setText(settext);
		tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		return(tv);
	}
}
