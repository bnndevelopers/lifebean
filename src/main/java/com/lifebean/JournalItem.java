package com.lifebean;

public class JournalItem
{
	private String type;
	private String title;

	public JournalItem(String type, String title) {
		this.type = type;
		this.title = title;
	}

	public String getType() {
		return(type);
	}

	public String getTitle() {
		return(title);
	}
}
