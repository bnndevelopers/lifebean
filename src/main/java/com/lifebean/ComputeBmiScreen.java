package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Button;
import android.content.Intent;
import android.view.View;
import android.app.DatePickerDialog;
import android.widget.TextView;
import java.util.Calendar;
import android.graphics.drawable.ColorDrawable;
import android.graphics.Color;
import android.widget.DatePicker;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.widget.Toast;
import android.database.Cursor;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.view.Gravity;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.LayoutInflater;
import android.text.Editable;
import android.text.TextWatcher;


public class ComputeBmiScreen extends AppCompatActivity
{
	private EditText height;
	private EditText weight;
	private TextView result;
	private Button calculate;
	private Button btnSave;
	private Button btnViewBmi;
	private Button btnUpdate;
	private Button btnDelete;
	private EditText etID;
	private HealthDatabaseHelper bmiDB;
	private TextView mDisplayDate;
	private DatePickerDialog.OnDateSetListener mDateSetListener;
	private Calendar calendar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bmi_main);
		Toolbar mToolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("BMI Calculator");
		bmiDB = new HealthDatabaseHelper(this);
		height = (EditText) findViewById(R.id.height);
		weight = (EditText) findViewById(R.id.weight);
		calculate = (Button) findViewById(R.id.calc);
		btnSave = (Button) findViewById(R.id.btnSave);
		btnViewBmi = (Button) findViewById(R.id.btnViewBmi);
		btnUpdate = (Button) findViewById(R.id.btnUpdate);
		btnDelete = (Button) findViewById(R.id.btnDel);
		result = (TextView) findViewById(R.id.result);
		mDisplayDate = (TextView)findViewById(R.id.tvcal);
		etID = (EditText)findViewById(R.id.etID);
		mDisplayDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Calendar cal = Calendar.getInstance();
				int year = cal.get(Calendar.YEAR);
				int month = cal.get(Calendar.MONTH);
				int day = cal.get(Calendar.DAY_OF_MONTH);
				DatePickerDialog dialog = new DatePickerDialog(ComputeBmiScreen.this,android.R.style.Theme_Holo_Light_Dialog_MinWidth,mDateSetListener,year,month,day);
				dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				dialog.show();
			}
		});
		mDateSetListener = new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker datePicker, int year, int month, int day) {
				month = month + 1;
				String date = month + "/" + day + "/" + year;
				mDisplayDate.setText(date);
			}
		};
		calculate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				calculateBMI();
			}
		});
		saveData();
		viewBmiHistory();
		updateData();
		DeleteData();
	}

	public void calculateBMI() {
		String heightStr = height.getText().toString();
		String weightStr = weight.getText().toString();

		if (heightStr != null && !"".equals(heightStr) && weightStr != null && !"".equals(weightStr)) {
			float heightValue = Float.parseFloat(heightStr) / 100;
			float weightValue = Float.parseFloat(weightStr);
			float bmi = weightValue / (heightValue * heightValue);
			displayBMI(bmi);
		}
	}

	public void displayBMI(float bmi) {
		String bmilabel = "";
		if(Float.compare(bmi, 15f) <= 0) {
			bmilabel = getString(R.string.very_severely_underweight);
		}
		else if(Float.compare(bmi, 15f) > 0 && (Float.compare(bmi, 16f)) <= 0) {
			bmilabel = getString(R.string.severely_underweight);
		}
		else if(Float.compare(bmi, 16f) > 0 && (Float.compare(bmi, 18.5f)) <= 0) {
			bmilabel = getString(R.string.underweight);
		}
		else if(Float.compare(bmi, 18.5f) > 0 && (Float.compare(bmi, 24.9f)) <= 0) {
			bmilabel = getString(R.string.normal);
		}
		else if(Float.compare(bmi, 25f) > 0 && (Float.compare(bmi, 29.9f)) <= 0) {
			bmilabel = getString(R.string.overweight);
		}
		else if(Float.compare(bmi, 30f) > 0 && (Float.compare(bmi, 34.9f)) <= 0) {
			bmilabel = getString(R.string.obese_class_i);
		}
		else if(Float.compare(bmi, 35f) > 0 && (Float.compare(bmi, 39.9f)) <= 0) {
			bmilabel = getString(R.string.obese_class_ii);
		}
		else {
			bmilabel = getString(R.string.obese_class_iii);
		}
		bmilabel = bmi + "\n\n" + bmilabel;
		result.setText(bmilabel);
		result.setGravity(Gravity.CENTER);
		String res = result.getText().toString();
		Intent intent = new Intent(ComputeBmiScreen.this, ResultScreen.class);
		intent.putExtra("RESULT", res);
		startActivity(intent);
	}

	public void saveData() {
		btnSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String dt = mDisplayDate.getText().toString();
				String ht = height.getText().toString();
				String wt = weight.getText().toString();
				String bmiresult = result.getText().toString();
				if (dt != null && !"".equals(dt) && ht != null && !"".equals(ht) && wt != null && !"".equals(wt)) {
					boolean insertData = bmiDB.addData(dt, ht, wt, bmiresult);
					if(insertData == true) {
						Toast.makeText(ComputeBmiScreen.this, "BMI Recorded", Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(ComputeBmiScreen.this, "Something went wrong", Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(ComputeBmiScreen.this, "Please complete the details", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void viewBmiHistory() {
		btnViewBmi.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Cursor data = bmiDB.showData();
				if(data.getCount() == 0) {
					display("Error", "No data found.");
					return;
				}
				StringBuffer buffer = new StringBuffer();
				while(data.moveToNext()) {
					buffer.append("ID: " + data.getString(0) + "\n");
					buffer.append("Date taken: " + data.getString(1) + "\n");
					buffer.append("Height (cm): " + data.getString(2) + "\n");
					buffer.append("Weight (kg): " + data.getString(3) + "\n");
					buffer.append("BMI Result: " + data.getString(4) + "\n");
				}
				display("BMI RECORDS:", buffer.toString());
			}
		});
	}

	public void updateData() {
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = etID.getText().toString().length();
				if(temp > 0) {
					boolean update = bmiDB.updateData(etID.getText().toString(),mDisplayDate.getText().toString(),height.getText().toString(),weight.getText().toString());
					if(update == true) {
						Toast.makeText(ComputeBmiScreen.this, "Succsessfully updated record",Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(ComputeBmiScreen.this, "Something went wrong",Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(ComputeBmiScreen.this, "Enter an ID to update",Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public void display(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.show();
	}

	public void DeleteData() {
		btnDelete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int temp = etID.getText().toString().length();
				if(temp > 0) {
					Integer deleteRow = bmiDB.deleteData(etID.getText().toString());
					if(deleteRow > 0) {
						Toast.makeText(ComputeBmiScreen.this,"Successfully deleted a record",Toast.LENGTH_LONG).show();
					}
					else {
						Toast.makeText(ComputeBmiScreen.this,"Something went wrong",Toast.LENGTH_LONG).show();
					}
				}
				else {
					Toast.makeText(ComputeBmiScreen.this, "You must enter an ID to delete", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}
