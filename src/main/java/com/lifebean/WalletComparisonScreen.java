package com.lifebean;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.graphics.Color;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.widget.TextView;
import android.view.Gravity;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ScrollView;

public class WalletComparisonScreen extends AppCompatActivity
{
	int pretotal = 0;
	int curtotal = 0;
	Date date = Calendar.getInstance().getTime();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout layout = new LinearLayout(this);
		TextViewMaker tvm = new TextViewMaker(this);
		layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		layout.setOrientation(LinearLayout.VERTICAL);
		LayoutInflater inflater = LayoutInflater.from(this);
		ScrollView scroll = new ScrollView(this);
		Toolbar toolbar = (Toolbar)inflater.inflate(R.layout.toolbar_layout, null);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle("Compare to Last Month's Expense");
		toolbar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		/*Current Date TextView*/
		LinearLayout screenyear = new LinearLayout(this);
		LinearLayout.LayoutParams yearwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		yearwrap.setMargins(30,0,0,0);
		TextView year = tvm.createTextView(createDate());
		year.setTextSize(20);
		screenyear.setLayoutParams(yearwrap);
		screenyear.addView(year);
		/*Previous Month Expenses*/
		LinearLayout screenprevious = new LinearLayout(this);
		LinearLayout.LayoutParams previouswrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		previouswrap.setMargins(30,20,0,0);
		TextView previous = tvm.createTextView("Previous Month Expenses: ");
		previous.setTextSize(18);
		screenprevious.setLayoutParams(previouswrap);
		screenprevious.addView(previous);
		/*Total Previous Month Expenses*/
		LinearLayout screentotalprevious = new LinearLayout(this);
		LinearLayout.LayoutParams previoustotalwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		previoustotalwrap.setMargins(30,0,0,20);
		TextView previoustotal = tvm.createTextView("Previous Month Total");
		previoustotal.setTextSize(15);
		screentotalprevious.setLayoutParams(previoustotalwrap);
		screentotalprevious.addView(previoustotal);
		/*Current Month Expenses*/
		LinearLayout screencurrent = new LinearLayout(this);
		LinearLayout.LayoutParams currentwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		currentwrap.setMargins(30,0,0,0);
		TextView current = tvm.createTextView("Current Month Expenses: ");
		current.setTextSize(18);
		screencurrent.setLayoutParams(currentwrap);
		screencurrent.addView(current);
		/*Total Current Month Expenses*/
		LinearLayout screentotalcurrent = new LinearLayout(this);
		LinearLayout.LayoutParams totalcurrentwrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		totalcurrentwrap.setMargins(30,0,0,0);
		TextView totalcurrent = tvm.createTextView("Current Month Total");
		totalcurrent.setTextSize(15);
		screentotalcurrent.setLayoutParams(totalcurrentwrap);
		screentotalcurrent.addView(totalcurrent);
		layout.addView(toolbar);
		layout.addView(screenyear);
		layout.addView(screenprevious);
		layout.addView(screentotalprevious);
		layout.addView(screencurrent);
		layout.addView(screentotalcurrent);
		scroll.addView(layout);
		setContentView(scroll);
	}

	public String createDate() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		String formattedDate = df.format(date);
		return(formattedDate);
	}

	public ImageView createImage() {
		ImageView icon = new ImageView(this);
		icon.setImageResource(R.drawable.pig);
		return(icon);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == android.R.id.home) {
			finish();
		}
		return(super.onOptionsItemSelected(item));
	}
}